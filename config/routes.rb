Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root to: 'quotes#index'

  resources :quotes do
    resources :items, except: [:index]
  end

end
