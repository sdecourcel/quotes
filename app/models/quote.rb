class Quote < ApplicationRecord
  has_many :items, dependent: :destroy

  validates :name, presence: true

  monetize :pre_tax_total_price_cents
  monetize :total_price_cents
  monetize :total_tax_cents

  def pre_tax_total_price_cents
    items.sum(&:pre_tax_total_price_cents)
  end

  def total_price_cents
    items.sum(&:total_price_cents)
  end

  def total_tax_cents
    total_price_cents - pre_tax_total_price_cents
  end
end
