class Item < ApplicationRecord
  TVA = 1.20

  belongs_to :quote

  validates :name, presence: true
  validates :quantity,
            presence: true,
            numericality: {
              only_integer: true,
              greater_than: 0
            }
  validates :pre_tax_unit_price_cents,
            presence: true,
            numericality: {
              only_integer: true,
              greater_than_or_equal_to: 0
            }

  monetize :pre_tax_unit_price_cents,
            allow_nil: false,
            numericality: { greater_than_or_equal_to: 0 }
  monetize :pre_tax_total_price_cents,
            allow_nil: false,
            numericality: { greater_than_or_equal_to: 0 }
  monetize :total_price_cents,
            allow_nil: false,
            numericality: { greater_than_or_equal_to: 0 }

  scope :ordered, -> { order(id: :asc) }

  before_validation :calculate_totals

  private

  def calculate_totals
    calculate_pre_tax_total
    calculate_total
  end

  def calculate_pre_tax_total
    self.pre_tax_total_price_cents = pre_tax_unit_price_cents * quantity
  end

  def calculate_total
    self.total_price_cents = (pre_tax_total_price_cents * TVA).round
  end
end
