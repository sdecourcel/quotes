class QuotesController < ApplicationController
  before_action :set_quote, except: [:index, :new, :create]

  def index
    @quotes = Quote.all.order(created_at: :desc)
    @quote = Quote.new
  end

  def show
  end

  def new
    @quote = Quote.new
  end

  def create
    @quote = Quote.new(quote_params)

    if @quote.save
      respond_to do |format|
        format.turbo_stream { render turbo_stream: turbo_stream.prepend(:quotes, partial: "quotes/quote", locals: { quote: @quote } )}
        format.html { redirect_to quotes_path }
      end
    else
      render :new
    end

  end

  def edit
  end

  def update
      if @quote.update(quote_params)
        redirect_to quotes_path, notice: "Quote was successfully updated."
      else
        render :edit
      end
  end


  def destroy
    @quote.destroy

    respond_to do |format|
      format.turbo_stream { render turbo_stream: turbo_stream.remove(@quote) }
      format.html { redirect_to quotes_path }
    end
  end

  private

  def quote_params
    params.require(:quote).permit(:name)
  end

  def set_quote
    @quote = Quote.eager_load(:items).merge(Item.ordered).find(params[:id])
  end

end