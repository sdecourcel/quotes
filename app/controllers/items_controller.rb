class ItemsController < ApplicationController
  before_action :set_quote
  before_action :set_item, only: [:show, :edit, :update, :destroy]


  def show
  end

  def new
    @item = Item.new
  end

  def create
    @item = @quote.items.build(item_params)

    if @item.save
      respond_to do |format|
        format.turbo_stream
        format.html { redirect_to @quote }
      end
    else
      render :new
    end
  end

  def edit
  end

  def update

    if @item.update(item_params)
      respond_to do |format|
        format.turbo_stream
        format.html { redirect_to @quote }
      end
    else
      render :edit
    end
  end


  def destroy
    @item.destroy

    respond_to do |format|
      format.turbo_stream
      format.html { redirect_to @quote }
    end
  end

  private

  def item_params
    params.require(:item).permit(:name, :description, :quantity, :pre_tax_unit_price)
  end

  def set_quote
    @quote = Quote.find(params[:quote_id])
  end

  def set_item
    @item = @quote.items.find(params[:id])
  end

end