# Rails 7 Hotwire (Turbo + Stimulus) reactive single-page application
Reactive single-page application thanks to turbo-rails library now included by default in Rails 7, without having to write a single line of custom JavaScript.

## App running here :
https://turbo-quotes.herokuapp.com/


## Run locally :

* rails ./bin/setup
* rails server

=> app running on http://localhost:3000

## Following Turbo Rails tutorial authored by Alexandre Ruban
https://www.hotrails.dev/turbo-rails


## TODO

* Add RSPEC tests : update methods, requests tests with Capybara...
* Responsive + Improve UX...
* Add Devise + quote title validation unique per user
