class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items do |t|
      t.string :name, null: false
      t.text :description
      t.integer :quantity, null: false
      t.monetize :pre_tax_unit_price, null: false, amount: { null: false, default: 0 }
      t.monetize :pre_tax_total_price, null: false, amount: { null: false, default: 0 }
      t.monetize :total_price, null: false, amount: { null: false, default: 0 }
      t.references :quote, null: false, foreign_key: true, index: true

      t.timestamps
    end
  end
end
