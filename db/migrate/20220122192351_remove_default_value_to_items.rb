class RemoveDefaultValueToItems < ActiveRecord::Migration[7.0]
  def change
    change_column_default( :items, :pre_tax_unit_price_cents, from: 0, to: nil )
  end
end
