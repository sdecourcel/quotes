FactoryBot.define do
  factory :item do
    name { "Faker::Lorem.words" }
    description { "Faker::Lorem.sentence" }
    quantity { 2 }
    pre_tax_unit_price_cents { 100 }
    pre_tax_total_price_cents { 200 }
    total_price_cents { 240 }
    quote { create(:quote) }
  end
end
