FactoryBot.define do
  factory :quote do
    name { Faker::Lorem.word }
  end
end
