require 'rails_helper'

RSpec.describe Item, type: :model do
  describe "::new" do

    subject { build(:item) }

    context "with valid attributes" do
      it { is_expected.to be_valid }
    end

    context "with invalid attributes" do
      it "return an error without a quote" do
        subject.quote = nil
        subject.valid?
        expect(subject.errors).to include(:quote)
      end

      it "return an error without an name" do
        subject.name = nil
        subject.valid?
        expect(subject.errors).to include(:name)
      end

      it "return an error without an quantity" do
        subject.quantity = nil
        subject.valid?
        expect(subject.errors).to include(:quantity)
      end

      it "return an error without an pre_tax_unit_price_cents" do
        subject.pre_tax_unit_price_cents = nil
        subject.valid?
        expect(subject.errors).to include(:pre_tax_unit_price_cents)
      end

      it "return an error without an pre_tax_total_price_cents" do
        subject.pre_tax_total_price_cents = nil
        subject.valid?
        expect(subject.errors).to include(:pre_tax_total_price_cents)
      end

      it "return an error without an total_price_cents" do
        subject.total_price_cents = nil
        subject.valid?
        expect(subject.errors).to include(:total_price_cents)
      end
    end

  end
end
