require 'rails_helper'

RSpec.describe Quote, type: :model do
  describe "::new" do

    subject { build(:quote) }

    context "with valid attributes" do
      it { is_expected.to be_valid }
    end

    context "with invalid attributes" do
      it "return an error without a name" do
        subject.name = nil
        subject.valid?
        expect(subject.errors).to include(:name)
      end
    end

  end
end
